<?php namespace EmailCaixa;

use Illuminate\Database\Eloquent\Model;

class Service extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'services';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['service_date', 'service_time', 'service_description', 'agency_id', 'partner_id', 'technician_id', 'statu_id'];
	
	public function agency()
    {
        return $this->belongsTo('EmailCaixa\Agency');
    }
	
	public function partner()
    {
        return $this->belongsTo('EmailCaixa\Partner');
    }
	
	public function technician()
    {
        return $this->belongsTo('EmailCaixa\Technician');
    }
	
	public function statu()
    {
        return $this->belongsTo('EmailCaixa\Statu');
    }
	
	public static function technicians_date($id, $fecha){
    	return Service::where('technician_id', '=', $id)->where('service_date', '=',  $fecha)->get();
    }
	
}
