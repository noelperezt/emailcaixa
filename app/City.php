<?php namespace EmailCaixa;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cities';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['city_name ', 'state_id', 'description ', 'statu_id '];
	
	public function statu()
    {
        return $this->belongsTo('EmailCaixa\Statu');
    }
	public function state()
    {
        return $this->belongsTo('EmailCaixa\State');
    }
	
	public static function cities($id){
    	return City::where('state_id','=',$id)
    	->get();
    }
	

}
