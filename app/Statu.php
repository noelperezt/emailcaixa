<?php namespace EmailCaixa;

use Illuminate\Database\Eloquent\Model;

class Statu extends Model {

	protected $table = 'status';
	
	protected $fillable = ['status_name', 'description'];

}
