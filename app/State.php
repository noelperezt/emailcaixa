<?php namespace EmailCaixa;

use Illuminate\Database\Eloquent\Model;

class State extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'states';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['state_name ', 'description ', 'statu_id'];
	
	public function statu()
    {
        return $this->belongsTo('EmailCaixa\Statu');
    }

}
