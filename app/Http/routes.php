<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('/', 'UserController@login');
Route::resource('User/login', 'UserController@login');
Route::resource('log', 'LogController@store');
Route::resource('logout', 'LogController@logOut');
Route::resource('logout', 'LogController@logOut');
Route::get('Home', 'HomeController@index');

/* User */
Route::resource('User', 'UserController@index');
Route::resource('User/create', 'UserController@create');
Route::post('User/create','UserController@create');
Route::resource('User/edit', 'UserController@edit');
Route::post('User/edit/{id}','UserController@edit');
Route::get('User/edit/{id}','UserController@edit');
Route::post('User/update','UserController@update');
Route::post('User/updateuser','UserController@updateuser');
Route::get('User/destroy/{id}','UserController@destroy');
Route::get('User/active/{id}','UserController@active');
Route::get('User/recuperarsenha/{user}/{email}','UserController@getUser');

/* Agency */
Route::resource('Agency', 'AgencyController@index');
Route::resource('Agency/create', 'AgencyController@create');
Route::post('Agency/create','AgencyController@create');
Route::resource('Agency/edit', 'AgencyController@edit');
Route::post('Agency/edit/{id}','AgencyController@edit');
Route::get('Agency/edit/{id}','AgencyController@edit');
Route::post('Agency/update','AgencyController@update');
Route::get('Agency/destroy/{id}','AgencyController@destroy');
Route::get('Agency/active/{id}','AgencyController@active');

/* City */
Route::resource('City', 'CityController@index');
Route::resource('City/create', 'CityController@create');
Route::post('City/create','CityController@create');
Route::resource('City/edit', 'CityController@edit');
Route::post('City/edit/{id}','CityController@edit');
Route::get('City/edit/{id}','CityController@edit');
Route::post('City/update','CityController@update');
Route::get('City/cities/{id}','CityController@getCities');
Route::get('City/destroy/{id}','CityController@destroy');
Route::get('City/active/{id}','CityController@active');

/* State */
Route::resource('State', 'StateController@index');
Route::resource('State/create', 'StateController@create');
Route::post('State/create','StateController@create');
Route::resource('State/edit', 'StateController@edit');
Route::post('State/edit/{id}','StateController@edit');
Route::get('State/edit/{id}','StateController@edit');
Route::post('State/update','StateController@update');
Route::get('State/destroy/{id}','StateController@destroy');
Route::get('State/active/{id}','StateController@active');

/* Service */
Route::resource('Service', 'ServiceController@index');
Route::resource('Service/create', 'ServiceController@create');
Route::post('Service/create','ServiceController@create');
Route::get('Service/create','ServiceController@create');
Route::resource('Service/edit', 'ServiceController@edit');
Route::post('Service/edit/{id}','ServiceController@edit');
Route::get('Service/edit/{id}','ServiceController@edit');
Route::post('Service/update','ServiceController@update');
Route::get('Service/partners/{id}','ServiceController@getPartnersCity');
Route::get('Service/partnerstate/{id}','ServiceController@getPartnerState');
Route::get('Service/technicians/{id}','ServiceController@getTechnician');
Route::get('Service/technicians_date/{id}/{fecha}','ServiceController@getService');
Route::get('Service/success/{id}','ServiceController@success');
Route::get('Service/cancel/{id}','ServiceController@cancel');

/* Partner */
Route::resource('Partner', 'PartnerController@index');
Route::resource('Partner/create', 'PartnerController@create');
Route::post('Partner/create','PartnerController@create');
Route::resource('Partner/edit', 'PartnerController@edit');
Route::post('Partner/edit/{id}','PartnerController@edit');
Route::get('Partner/edit/{id}','PartnerController@edit');
Route::post('Partner/update','PartnerController@update');
Route::get('Partner/destroy/{id}','PartnerController@destroy');
Route::get('Partner/active/{id}','PartnerController@active');

/* Technician */
Route::resource('Technician', 'TechnicianController@index');
Route::resource('Technician/create', 'TechnicianController@create');
Route::post('Technician/create','TechnicianController@create');
Route::resource('Technician/edit', 'TechnicianController@edit');
Route::post('Technician/edit/{id}','TechnicianController@edit');
Route::get('Technician/edit/{id}','TechnicianController@edit');
Route::post('Technician/update','TechnicianController@update'); 
Route::get('Technician/destroy/{id}','TechnicianController@destroy');
Route::get('Technician/active/{id}','TechnicianController@active'); 
Route::get('Technician/partners/{id}','TechnicianController@getPartnersCity');

//Route::get('home', 'HomeController@index');
/*Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);*/
