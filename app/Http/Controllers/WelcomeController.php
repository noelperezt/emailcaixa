<?php namespace EmailCaixa\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use GuzzleHttp\Client;


class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		if ($request->isMethod('post')) {
			$nomusu = strtoupper($request->input('NOMUSU'));
			$interno = $request->input('INTERNO');
			$body_xml="<serviceRequest serviceName='MobileLoginSP.login'>
			<requestBody><NOMUSU>".$nomusu."</NOMUSU><INTERNO>".$interno."</INTERNO></requestBody>
			</serviceRequest>";
			$client = new Client();
			$options = [
				'headers' => [
					'ContentT-ype' => 'text/xml; charset=UTF8',
				],
				'body' => $body_xml,
			];
			$response = $client->request('POST', 'http://fazendasaopedro.ddns.net:8280/mge/service.sbr?serviceName=MobileLoginSP.login', $options);
			
			$xml_response = simplexml_load_string($response->getBody()->getContents());
			$json_response = json_encode($xml_response);
			$decoded_response = json_decode($json_response,TRUE);
			//print_r($decoded_response);
			$status_response= $decoded_response['@attributes']['status'];
			if($status_response==1){
				return view('home');
			}else{
				return view('welcome');
			}
		}else{
			return view('welcome');
		}
	}
	
}
