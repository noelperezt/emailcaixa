<?php namespace EmailCaixa\Http\Controllers;

use EmailCaixa\Http\Requests;
use EmailCaixa\Http\Controllers\Controller;
use Auth;
use Session;
use Redirect;
use Mail;
use Illuminate\Http\Request;

class ServiceController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check())
        {
			$services= \EmailCaixa\Service::All();
			return view('service.index', compact('services'));
		}
		return Redirect::to('/');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		if (Auth::check())
        {	
			if ($request->isMethod('post')) {
								
				$service = new \EmailCaixa\Service;
				$service->service_date = $request['service_date'];
				$service->description = $request['description'];
				$service->service_time  = $request['service_time'];
				$service->service_description = strtoupper($request['service_description']);
				$service->agency_id  = $request['agency_id'];
				$service->partner_id  = $request['partner_id'];
				$service->technician_id  = $request['technician_id'];
				$service->statu_id = '3';
				
				if($service->save()){
					
					$agency = \DB::table('agencies')->where('id', $request['agency_id'])->first();
					$technician = \DB::table('technicians')->where('id', $request['technician_id'])->first();
					$city = \DB::table('cities')->where('id', $agency->city_id)->first();
					$email=Auth::user()->email;
					$data = array( 
					'cgc' => $agency->cgc, 
					'cep' => $agency->cep, 
					'unity' => $agency->unity, 
					'address' => $agency->address, 
					'neighborhood' => $agency->neighborhood, 
					'uf' => $agency->uf, 
					'city_name' => $city->city_name, 
					'technician_name' => $technician->technician_name, 
					'rg' => $technician->rg, 
					'date' => $request['service_date']);
					
					Mail::send('emails.services', $data, function($msj) use($email ){
						$msj->subject('AUTORIZAÇÃO DE ENTRADA');
						$msj->to( $email );
						$msj->cc('s.gil@resulthti.com.br');
					});
					
					
					Session::flash('message','green, Pedido criado corretamente');	
					return redirect('Service');
				}else{
					Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
					return redirect('service.create');
				}
		
			}else{	
				$services= \EmailCaixa\Service::where('statu_id', 3)->limit(5)->get();
				$agencies = \EmailCaixa\Agency::All();
				return view('service.create', compact('agencies', 'services'));
			
			}
		}
		return Redirect::to('/');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::check())
        {	
			$service= \EmailCaixa\Service::find($id);
			$status= \EmailCaixa\Statu::All();
			return view('service.edit', compact(array('service', 'status')));
		}
		return Redirect::to('/');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		if (Auth::check())
        {
			$service_update = \EmailCaixa\Service::find($request['id_service']);
			$service_update->description = strtoupper($request['service_update']);
			$service_update->statu_id = $request['statu_id'];
			if($service_update->save()){
				Session::flash('message','green, Pedido atualizado corretamente');	
				return redirect('Service');
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				return redirect('Service');
			}
		}
		return Redirect::to('/');
	}
		
	public function getPartnersCity(Request $request, $id){
        if($request->ajax()){
            $partner= \EmailCaixa\Agency::find($id);
			$city_partner = \DB::table('partners')->where('city_id', $partner->city_id)->first();
			if($city_partner){
				$partners = \EmailCaixa\Partner::partners_city($partner->city_id);
				return response()->json($partners);
			}else{
				return "state";
			}
			
        }
    }
	
	public function getPartnerState(Request $request, $id){
        if($request->ajax()){
            
			$partner= \EmailCaixa\Agency::find($id);
			$partners = \EmailCaixa\Partner::partners_state($partner->state_id);
		
			return response()->json($partners);
			
        }
    }
	
	public function getTechnician(Request $request, $id){
        if($request->ajax()){
            
			$partners = \EmailCaixa\Partner::partners_technician($id);
			return response()->json($partners);
			
        }
    }
	
	public function getService(Request $request, $id, $fecha){
		$fecha = str_replace('M', '/', $fecha);
        if($request->ajax()){           
			$services = \EmailCaixa\Service::technicians_date($id, $fecha);
			return response()->json($services);
			
        }
    }
	
	public function success(Request $request, $id)
	{
		if($request->ajax()){
			
			$service = \EmailCaixa\Service::find($id);
			$service->statu_id = "4";
			if($service->save()){
				Session::flash('message','green, Pedido atualizado corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
	
	public function cancel(Request $request, $id)
	{
		if($request->ajax()){
			
			$service = \EmailCaixa\Service::find($id);
			$service->statu_id = "5";
			if($service->save()){
				Session::flash('message','green, Pedido atualizado corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
	
	

}
