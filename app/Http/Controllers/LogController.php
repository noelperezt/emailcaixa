<?php namespace EmailCaixa\Http\Controllers;

use EmailCaixa\Http\Requests;
use Auth;
use Session;
use Redirect;
use Input;
use EmailCaixa\Http\Requests\LoginRequest;
use EmailCaixa\Http\Controllers\Controller;

use Illuminate\Http\Request;

class LogController extends Controller {


	public function store(LoginRequest $request)
	{
		
		// Guardamos en un arreglo los datos del usuario.
        $userdata = array(
            'username' => strtoupper(Input::get('username')),
            'password'=> Input::get('password'),
			'statu_id'=>1
        );
        // Validamos los datos y además mandamos como un segundo parámetro la opción de recordar el usuario.
        if(Auth::attempt($userdata, Input::get('remember-me', 0)))
        {
            // De ser datos válidos nos mandara a la bienvenida
            return Redirect::to('Home');
        }
        // En caso de que la autenticación haya fallado manda un mensaje al formulario de login y también regresamos los valores enviados con withInput().
		Session::flash('message','red, Os dados inseridos estão incorretos.');
        return Redirect::to('/');
		
	}
	
	public function logOut()
    {
        Auth::logout();
		Session::flash('message','green, Sua sessão foi fechada.');
        return Redirect::to('/');
    }
	
	

}
