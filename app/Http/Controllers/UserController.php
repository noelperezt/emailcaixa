<?php namespace EmailCaixa\Http\Controllers;

use EmailCaixa\Http\Requests;
use EmailCaixa\Http\Controllers\Controller;
use Auth;
use Session;
use Mail;
use Redirect;
use Illuminate\Http\Request;

class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		if (Auth::check())
        {
			if(Auth::user()->role_id == 1){
				$users= \EmailCaixa\User::All();
				return view('user.index', compact('users'));
			}else{
				Session::flash('message','red, Não tem os privilégios necessários. Entre em contato com o administrador.');	
				return redirect('Home');
			}
		}
		return view('user.login');
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		if (Auth::check())
        {
			if(Auth::user()->role_id == 1){
				if ($request->isMethod('post')) {
					
					$doc_id = \DB::table('users')->where('document_number', strtoupper($request['document_number']))->first();
					if($doc_id){
						Session::flash('message','red, A identidade já existe.Verifique a informação.');	
						$roles= \EmailCaixa\Role::All();
						return view('user.create', compact('roles'));
					}else{
									
						$user = new \EmailCaixa\User;

						$user->first_name = ucwords($request['first_name']);
						$user->last_name = ucwords($request['last_name']);
						$user->username = strtoupper($request['username']);
						$user->password = bcrypt($request['password']);
						$user->email = strtolower($request['email']);
						$user->document_number = strtoupper($request['document_number']);
						$user->role_id =  $request['id_role'];
						$user->statu_id = '1';
						
						if($user->save()){
							Session::flash('message','green, Usuário criado corretamente');	
							return redirect('User');
						}else{
							Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
							return redirect('User/create');
						}
						
						
						
					}
						
				}else{	
					
					$roles= \EmailCaixa\Role::All();
					return view('user.create', compact('roles'));
				
				}
			}else{
				Session::flash('message','red, Não tem os privilégios necessários. Entre em contato com o administrador.');	
				return redirect('Home');
			}
		}
		return view('user.login');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::check())
        {
			if(Auth::user()->role_id == 1){
				$user= \EmailCaixa\User::find($id);
				$roles= \EmailCaixa\Role::All();
				$status= \EmailCaixa\Statu::All();
				return view('user.edit', compact(array('user', 'roles', 'status')));
			}else{
				if(Auth::user()->id == $id){
					$user= \EmailCaixa\User::find($id);
					$roles= \EmailCaixa\Role::All();
					$status= \EmailCaixa\Statu::All();
					return view('user.edit', compact(array('user', 'roles', 'status')));
				}else{
				
					Session::flash('message','red, Não tem os privilégios necessários. Entre em contato com o administrador.');	
					return redirect('Home');
				
				}
			}
		}
		return view('user.login');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		if (Auth::check())
        {
			$user_update = \EmailCaixa\User::find($request['id_user']);
			$user_update->first_name = ucwords($request['first_name']);
			$user_update->last_name = ucwords($request['last_name']);
			$user_update->username = strtoupper($request['username']);
			if($request['validapass']==1){
				$user_update->password = bcrypt($request['password']);
			}
			$user_update->email = strtolower($request['email']);
			$user_update->document_number = strtoupper($request['document_number']);
			$user_update->role_id = $request['id_role'];
			$user_update->statu_id = $request['id_statu'];
			if($user_update->save()){
				Session::flash('message','green, Usuário atualizado corretamente');	
				return redirect('User');
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				return redirect('User');
			}
		}
		return view('user.login');
	}
	
	public function updateuser(Request $request)
	{
		if (Auth::check())
        {
			$user_update = \EmailCaixa\User::find($request['id_user']);
			$user_update->first_name = ucwords($request['first_name']);
			$user_update->last_name = ucwords($request['last_name']);
			if($request['validapass']==1){
				$user_update->password = bcrypt($request['password']);
			}
			$user_update->email = strtolower($request['email']);
			$user_update->document_number = strtoupper($request['document_number']);
			if($user_update->save()){
				Session::flash('message','green, Usuário atualizado corretamente');	
				return redirect('Home');
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				return redirect('Home');
			}
		}
		return view('user.login');
	}

		
	public function login()
	{
		if (Auth::check())
        {
			return redirect('Home');
		}
		return view('user.login');
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id)
	{
		if($request->ajax()){
			
			$user = \EmailCaixa\User::find($id);
			$user->statu_id = "2";
			if($user->save()){
				Session::flash('message','green, Usuário atualizado corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
	
	public function active(Request $request, $id)
	{
		if($request->ajax()){
			
			$user = \EmailCaixa\User::find($id);
			$user->statu_id = "1";
			if($user->save()){
				Session::flash('message','green, Usuário atualizado corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
	
	public function getUser(Request $request, $user, $email){
        if($request->ajax()){
            
			$users = \DB::table('users')->where('username', strtoupper($user))->where('email', strtolower($email))->first();
			
			if($users){
				 
				$password= $this->digits();
				$user = \EmailCaixa\User::find($users->id);
				$user->password = bcrypt($password);
				if($user->save()){
					
					$data = array( 
					'first_name' => $users->first_name, 
					'last_name' => $users->last_name, 
					'email' => $users->email,
					'password' => $password, 
					'username' => $users->username );
					
					Mail::send('emails.resetpassword', $data, function($msj) use($email ){
						$msj->subject('Recuperar Senha - Alfa Telecom');
						$msj->to( $email );
					});
				}
				
				$result=1;
				return response()->json($result);
				
				
			}else{
				
				$result=0;
				return response()->json($result);
				
			}
						
        }
    }
	
	public static function digits(){
        $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $longitud_cadena = strlen($cadena);
        $codigo = "";
        $longitud = 8;
        for($i = 1; $i <= $longitud; $i++){
            $pos=rand(0,$longitud_cadena-1);
            $codigo .= substr($cadena,$pos,1);
        }

        return $codigo;
    }
		

}
