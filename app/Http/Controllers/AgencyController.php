<?php namespace EmailCaixa\Http\Controllers;

use EmailCaixa\Http\Requests;
use EmailCaixa\Http\Controllers\Controller;
use Auth;
use Session;
use Redirect;
use Illuminate\Http\Request;

class AgencyController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	
		if (Auth::check())
        {
			$agencies= \EmailCaixa\Agency::All();
			return view('agency.index', compact('agencies'));
		}
		return Redirect::to('/');
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		
		if (Auth::check())
        {
			if ($request->isMethod('post')) {
			
				$doc_id = \DB::table('agencies')->where('cgc', strtoupper($request['cgc']))->first();
				if($doc_id){
					Session::flash('message','red, A Agencia já existe.Verifique a informação.');	
					return view('agency.create');
				}else{
								
					$agency = new \EmailCaixa\Agency;
					$agency->cgc = strtoupper($request['cgc']);
					$agency->agency_name = strtoupper($request['agency_name']);
					$agency->unity = strtoupper($request['unity']);
					$agency->address = strtoupper($request['address']);
					$agency->agency_number = strtoupper($request['agency_number']);
					$agency->neighborhood = strtoupper($request['neighborhood']);
					$agency->cep = strtoupper($request['cep']);
					$agency->city_id = $request['city_id'];
					$agency->uf = strtoupper($request['uf']);
					$agency->telephone = strtoupper($request['telephone']);
					$agency->state_id  = strtoupper($request['state_id']);
					$agency->statu_id = '1';
					
					if($agency->save()){
						Session::flash('message','green, Agencia criada corretamente');	
						return redirect('Agency');
					}else{
						Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
						return redirect('agency.create');
					}
		
				}
					
			}else{	
				
				$cities= \EmailCaixa\City::All();
				$states= \EmailCaixa\State::All();
				return view('agency.create', compact('cities', 'states'));
			
			}
		}
		return Redirect::to('/');
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		if (Auth::check())
        {
			$agency= \EmailCaixa\Agency::find($id);
			$cities= \EmailCaixa\City::All();
			$status= \EmailCaixa\Statu::All();
			$states= \EmailCaixa\State::All();
			return view('agency.edit', compact(array('agency', 'cities', 'status', 'states')));
		}
		return Redirect::to('/');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		if (Auth::check())
        {
			$agency = \EmailCaixa\Agency::find($request['id_agency']);
			$agency->agency_name = strtoupper($request['agency_name']);
			$agency->cgc = strtoupper($request['cgc']);
			$agency->unity = strtoupper($request['unity']);
			$agency->address = strtoupper($request['address']);
			$agency->agency_number = strtoupper($request['agency_number']);
			$agency->neighborhood = strtoupper($request['neighborhood']);
			$agency->cep = strtoupper($request['cep']);
			$agency->city_id = $request['city_id'];
			$agency->uf = strtoupper($request['uf']);
			$agency->telephone = strtoupper($request['telephone']);
			$agency->state_id = $request['state_id2'];
			$agency->statu_id = $request['id_statu'];
			if($agency->save()){
				Session::flash('message','green, Agencia atualizada corretamente');	
				return redirect('Agency');
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				return redirect('Agency');
			}
		}
		return Redirect::to('/');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id)
	{
		if($request->ajax()){
			
			$agency = \EmailCaixa\Agency::find($id);
			$agency->statu_id = "2";
			if($agency->save()){
				Session::flash('message','green, Agencia atualizada corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
	
	public function active(Request $request, $id)
	{
		if($request->ajax()){
			
			$agency = \EmailCaixa\Agency::find($id);
			$agency->statu_id = "1";
			if($agency->save()){
				Session::flash('message','green, Agencia atualizada corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
		
	
}
