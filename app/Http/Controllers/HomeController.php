<?php namespace EmailCaixa\Http\Controllers;

use EmailCaixa\Http\Requests;
use EmailCaixa\Http\Controllers\Controller;
use Auth;
use Session;
use Redirect;
use Illuminate\Http\Request;

class HomeController extends Controller {

	/**
	 * Display a principal page of app.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check())
        {
			$services= \EmailCaixa\Service::where('statu_id', 3)->limit(5)->get();
			return view('home', compact('services'));
		}
		return Redirect::to('/');
	}

}
