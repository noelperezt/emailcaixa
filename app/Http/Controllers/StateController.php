<?php namespace EmailCaixa\Http\Controllers;

use EmailCaixa\Http\Requests;
use EmailCaixa\Http\Controllers\Controller;
use Auth;
use Session;
use Redirect;
use Illuminate\Http\Request;

class StateController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check())
        {
			$states= \EmailCaixa\State::All();
			return view('state.index', compact('states'));
		}
		return Redirect::to('/');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		if (Auth::check())
        {
			if ($request->isMethod('post')) {
				
				$doc_id = \DB::table('states')->where('state_name', strtoupper($request['state_name']))->first();
				if($doc_id){
					Session::flash('message','red, O Estado já existe.Verifique a informação.');	
					return view('state.create');
				}else{
								
					$state = new \EmailCaixa\State;

					$state->state_name  = strtoupper($request['state_name']);
					$state->description = strtoupper($request['description']);
					$state->statu_id = '1';
					
					if($state->save()){
						Session::flash('message','green, Estado criado corretamente');	
						return redirect('State');
					}else{
						Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
						return redirect('state.create');
					}
					
					
					
				}
					
			}else{	
				
				return view('state.create');
			
			}
		}
		return Redirect::to('/');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::check())
        {
			$state= \EmailCaixa\State::find($id);
			$status= \EmailCaixa\Statu::All();
			return view('state.edit', compact(array('state', 'status')));
		}
		return Redirect::to('/');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		if (Auth::check())
        {
			$state_update = \EmailCaixa\State::find($request['id_state']);
			$state_update->state_name = strtoupper($request['state_name']);
			$state_update->description = strtoupper($request['description']);
			$state_update->statu_id = $request['id_statu'];
			if($state_update->save()){
				Session::flash('message','green, Estado atualizado corretamente');	
				return redirect('State');
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				return redirect('State');
			}
		}
		return Redirect::to('/');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id)
	{
		if($request->ajax()){
			
			$state = \EmailCaixa\State::find($id);
			$state->statu_id = "2";
			if($state->save()){
				Session::flash('message','green, Estado atualizado corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
	
	public function active(Request $request, $id)
	{
		if($request->ajax()){
			
			$state = \EmailCaixa\State::find($id);
			$state->statu_id = "1";
			if($state->save()){
				Session::flash('message','green, Estado atualizado corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}

}
