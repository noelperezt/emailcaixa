<?php namespace EmailCaixa\Http\Controllers;

use EmailCaixa\Http\Requests;
use EmailCaixa\Http\Controllers\Controller;
use Auth;
use Session;
use Redirect;
use Illuminate\Http\Request;

class TechnicianController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check())
        {
			$technicians= \EmailCaixa\Technician::All();
			return view('technician.index', compact('technicians'));
		}
		return view('user.login');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		if (Auth::check())
        {
			if ($request->isMethod('post')) {
				
				$doc_id = \DB::table('technicians')->where('rg', strtoupper($request['rg']))->first();
				if($doc_id){
					Session::flash('message','red, O Técnico já existe.Verifique a informação.');	
					return view('technician.create');
				}else{
								
					$technician = new \EmailCaixa\Technician;

					$technician->technician_name = strtoupper($request['technician_name']);
					$technician->rg = strtoupper($request['rg']);
					$technician->partner_id = $request['partner_id'];
					$technician->telephone = strtoupper($request['telephone']);
					$technician->statu_id = '1';
					$technician->state_id = $request['state_id'];
					$technician->city_id = $request['city_id'];
					
					if($technician->save()){
						Session::flash('message','green, Técnico criado corretamente');	
						return redirect('Technician');
					}else{
						Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
						return redirect('technician.create');
					}
		
				}
					
			}else{	
				$states= \EmailCaixa\State::All();
				return view('technician.create', compact('states'));
			
			}
		}
		return view('user.login');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::check())
        {

			$technician= \EmailCaixa\Technician::find($id);
			$status= \EmailCaixa\Statu::All();
			$states= \EmailCaixa\State::All();
			$cities = \DB::table('cities')->where('state_id', $technician->state_id)->get();
			$partners = \DB::table('partners')->where('city_id', $technician->city_id)->get();
			return view('technician.edit', compact(array('technician', 'states', 'status', 'partners', 'cities')));
		}
		return view('user.login');
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		if (Auth::check())
        {
			$technician = \EmailCaixa\Technician::find($request['id_technician']);
			$technician->technician_name = strtoupper($request['technician_name']);
			$technician->rg = strtoupper($request['rg']);
			$technician->partner_id = $request['partner_id'];
			$technician->telephone = strtoupper($request['telephone']);
			$technician->statu_id = $request['id_statu'];
			$technician->state_id = $request['state_id'];
			$technician->city_id = $request['city_id'];
			if($technician->save()){
				Session::flash('message','green, Técnico atualizadao corretamente');	
				return redirect('Technician');
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				return redirect('Technician');
			}
		}
		return view('user.login');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id)
	{
		if($request->ajax()){
			
			$technician = \EmailCaixa\Technician::find($id);
			$technician->statu_id = "2";
			if($technician->save()){
				Session::flash('message','green, Técnico atualizado corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
	
	public function active(Request $request, $id)
	{
		if($request->ajax()){
			
			$technician = \EmailCaixa\Technician::find($id);
			$technician->statu_id = "1";
			if($technician->save()){
				Session::flash('message','green, Técnico atualizado corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
	
	
	public function getPartnersCity(Request $request, $id){
        if($request->ajax()){
            
			$partners = \EmailCaixa\Partner::partners_city($id);
			return response()->json($partners);
			
        }
    }
	

}
