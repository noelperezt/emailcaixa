<?php namespace EmailCaixa\Http\Controllers;

use EmailCaixa\Http\Requests;
use EmailCaixa\Http\Controllers\Controller;
use Auth;
use Session;
use Redirect;
use Illuminate\Http\Request;

class PartnerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check())
        {
			$partners= \EmailCaixa\Partner::All();
			return view('partner.index', compact('partners'));
		}
		return Redirect::to('/');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		if (Auth::check())
        {
			if ($request->isMethod('post')) {
				
				$doc_id = \DB::table('partners')->where('cep', strtoupper($request['cep']))->first();
				if($doc_id){
					Session::flash('message','red, O Parceiro já existe.Verifique a informação.');	
					return view('partner.create');
				}else{
								
					$partner = new \EmailCaixa\Partner;

					$partner->partner_name = strtoupper($request['partner_name']);
					$partner->address = strtoupper($request['address']);
					$partner->partner_number = strtoupper($request['partner_number']);
					$partner->neighborhood = strtoupper($request['neighborhood']);
					$partner->cep = strtoupper($request['cep']);
					$partner->city_id = $request['city_id'];
					$partner->uf = strtoupper($request['uf']);
					$partner->telephone = strtoupper($request['telephone']);
					$partner->celphone = strtoupper($request['celphone']);
					$partner->state_id = strtoupper($request['state_id']);
					$partner->statu_id = '1';
					
					if($partner->save()){
						Session::flash('message','green, Parceiro criado corretamente');	
						return redirect('Partner');
					}else{
						Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
						return redirect('partner.create');
					}
		
				}
					
			}else{	
				$states= \EmailCaixa\State::All();
				$cities= \EmailCaixa\City::All();
				return view('partner.create', compact('cities', 'states'));
			
			}
		}
		return Redirect::to('/');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::check())
        {

			$partner= \EmailCaixa\Partner::find($id);
			$cities= \EmailCaixa\City::All();
			$status= \EmailCaixa\Statu::All();
			$states= \EmailCaixa\State::All();
			return view('partner.edit', compact(array('partner', 'cities', 'status', 'states')));
		}
		return Redirect::to('/');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		if (Auth::check())
        {
			$partner = \EmailCaixa\Partner::find($request['id_partner']);
			$partner->partner_name = strtoupper($request['partner_name']);
			$partner->address = strtoupper($request['address']);
			$partner->partner_number = strtoupper($request['partner_number']);
			$partner->neighborhood = strtoupper($request['neighborhood']);
			$partner->cep = strtoupper($request['cep']);
			$partner->city_id = $request['city_id'];
			$partner->uf = strtoupper($request['uf']);
			$partner->telephone = strtoupper($request['telephone']);
			$partner->celphone = strtoupper($request['celphone']);
			$partner->statu_id = $request['id_statu'];
			$partner->state_id = $request['state_id2'];
			if($partner->save()){
				Session::flash('message','green, Parceiro atualizadao corretamente');	
				return redirect('Partner');
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				return redirect('Partner');
			}
		}
		return Redirect::to('/');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id)
	{
		if($request->ajax()){
			
			$partner = \EmailCaixa\Partner::find($id);
			$partner->statu_id = "2";
			if($partner->save()){
				Session::flash('message','green, Parceiro atualizado corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
	
	public function active(Request $request, $id)
	{
		if($request->ajax()){
			
			$partner = \EmailCaixa\Partner::find($id);
			$partner->statu_id = "1";
			if($partner->save()){
				Session::flash('message','green, Parceiro atualizado corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
}