<?php namespace EmailCaixa\Http\Controllers;

use EmailCaixa\Http\Requests;
use EmailCaixa\Http\Controllers\Controller;
use Auth;
use Session;
use Redirect;
use Illuminate\Http\Request;

class CityController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check())
        {
			$cities= \EmailCaixa\City::All();
			return view('city.index', compact('cities'));
		}
		return Redirect::to('/');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		if (Auth::check())
        {
			if ($request->isMethod('post')) {
				
				$doc_id = \DB::table('cities')->where('city_name', strtoupper($request['city_name']))->first();
				if($doc_id){
					Session::flash('message','red, A Cidade já existe.Verifique a informação.');	
					return view('city.create');
				}else{
								
					$city = new \EmailCaixa\City;

					$city->city_name  = strtoupper($request['city_name']);
					$city->state_id  = strtoupper($request['state_id']);
					$city->description = strtoupper($request['description']);
					$city->statu_id = '1';
					
					if($city->save()){
						Session::flash('message','green, Cidade criada corretamente');	
						return redirect('City');
					}else{
						Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
						return redirect('city.create');
					}
					
					
					
				}
					
			}else{	
				
				$states= \EmailCaixa\State::All();
				return view('city.create', compact('states'));
			
			}
		}
	}

	
	public function getCities(Request $request, $id){
        if($request->ajax()){
            $cities = \EmailCaixa\City::cities($id);
            return response()->json($cities);
        }
    }
	

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::check())
        {

			$city= \EmailCaixa\City::find($id);
			$states= \EmailCaixa\State::All();
			$status= \EmailCaixa\Statu::All();
			return view('city.edit', compact(array('city', 'status', 'states')));
		}
		return Redirect::to('/');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		if (Auth::check())
        {
			$city_update = \EmailCaixa\City::find($request['id_city']);
			$city_update->city_name = strtoupper($request['city_name']);
			$city_update->description = strtoupper($request['description']);
			$city_update->state_id = $request['state_id2'];
			$city_update->statu_id = $request['id_statu'];
			if($city_update->save()){
				Session::flash('message','green, Cidade atualizada corretamente');	
				return redirect('City');
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				return redirect('City');
			}
		}
		return Redirect::to('/');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id)
	{
		if($request->ajax()){
			
			$city = \EmailCaixa\City::find($id);
			$city->statu_id = "2";
			if($city->save()){
				Session::flash('message','green, Cidade atualizada corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}
	
	public function active(Request $request, $id)
	{
		if($request->ajax()){
			
			$city = \EmailCaixa\City::find($id);
			$city->statu_id = "1";
			if($city->save()){
				Session::flash('message','green, Cidade atualizada corretamente');	
				$result=1;
				return response()->json($result);
			}else{
				Session::flash('message','red, Ocorreu um problema. Verifique a informação.');	
				$result=0;
				return response()->json($result);
			}
		}
	}

}
