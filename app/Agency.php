<?php namespace EmailCaixa;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'agencies';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cgc', 'unity', 'address', 'agency_number', 'neighborhood', 'cep',  'city_id', 'uf', 'telephone', 'statu_id', 'state_id', 'agency_name'];
	
	public function city()
    {
        return $this->belongsTo('EmailCaixa\City');
    }
	public function statu()
    {
        return $this->belongsTo('EmailCaixa\Statu');
    }
	public function state()
    {
        return $this->belongsTo('EmailCaixa\State');
    }

}
