<?php namespace EmailCaixa;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'partners';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['partner_name', 'address', 'partner_number', 'neighborhood', 'cep',  'city_id', 'uf', 'telephone', 'celphone', 'statu_id', 'state_id'];
	
	public function city()
    {
        return $this->belongsTo('EmailCaixa\City');
    }
	public function statu()
    {
        return $this->belongsTo('EmailCaixa\Statu');
    }
	public function state()
    {
        return $this->belongsTo('EmailCaixa\State');
    }
	
	public static function partners_city($id){
    	return Partner::where('city_id','=',$id)
    	->get();
    }
	public static function partners_state($id){
    	return Partner::where('state_id','=',$id)
    	->get();
    }
	public static function partners_technician($id){
    	return Technician::where('partner_id','=',$id)
    	->get();
    }
	
}
