<?php namespace EmailCaixa;

use Illuminate\Database\Eloquent\Model;

class Technician extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'technicians';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['technician_name', 'rg', 'telephone', 'partner_id', 'statu_id', 'state_id', 'city_id'];
	
	public function partner()
    {
        return $this->belongsTo('EmailCaixa\Partner');
    }
	public function statu()
    {
        return $this->belongsTo('EmailCaixa\Statu');
    }

}
