<?php namespace EmailCaixa;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

	protected $table = 'roles';
	
	protected $fillable = ['role_name', 'description'];

}
