$(".delete_agency").click(event => {
	var id_agency = event.target.id;
	$.get("../emailcaixa/Agency/destroy/"+id_agency+"", function(response, state){
		location.reload();
	});
});

$(".active_agency").click(event => {
	var id_agency = event.target.id;
	$.get("../emailcaixa/Agency/active/"+id_agency+"", function(response, state){
		location.reload();
	});
});

$(".delete_city").click(event => {
	var id_city = event.target.id;
	$.get("../emailcaixa/City/destroy/"+id_city+"", function(response, state){
		location.reload();
	});
});

$(".active_city").click(event => {
	var id_city = event.target.id;
	$.get("../emailcaixa/City/active/"+id_city+"", function(response, state){
		location.reload();
	});
});

$(".delete_state").click(event => {
	var id_state = event.target.id;
	$.get("../emailcaixa/State/destroy/"+id_state+"", function(response, state){
		location.reload();
	});
});

$(".active_state").click(event => {
	var id_state = event.target.id;
	$.get("../emailcaixa/State/active/"+id_state+"", function(response, state){
		location.reload();
	});
});

$(".delete_partner").click(event => {
	var id_partner = event.target.id;
	$.get("../emailcaixa/Partner/destroy/"+id_partner+"", function(response, state){
		location.reload();
	});
});

$(".active_partner").click(event => {
	var id_partner = event.target.id;
	$.get("../emailcaixa/Partner/active/"+id_partner+"", function(response, state){
		location.reload();
	});
});

$(".delete_technician").click(event => {
	var id_technician = event.target.id;
	$.get("../emailcaixa/Technician/destroy/"+id_technician+"", function(response, state){
		location.reload();
	});
});

$(".active_technician").click(event => {
	var id_technician = event.target.id;
	$.get("../emailcaixa/Technician/active/"+id_technician+"", function(response, state){
		location.reload();
	});
});

$(".delete_user").click(event => {
	var id_user = event.target.id;
	$.get("../emailcaixa/User/destroy/"+id_user+"", function(response, state){
		location.reload();
	});
});

$(".active_user").click(event => {
	var id_user = event.target.id;
	$.get("../emailcaixa/User/active/"+id_user+"", function(response, state){
		location.reload();
	});
});

$(".success_service").click(event => {
	var id_service = event.target.id;
	$.get("../emailcaixa/Service/success/"+id_service+"", function(response, state){
		location.reload();
	});
});
$(".cancel_service").click(event => {
	var id_service = event.target.id;
	$.get("../emailcaixa/Service/cancel/"+id_service+"", function(response, state){
		location.reload();
	});
});

$(document).ready(function(){

	$("#botonusuario").click(function(){
		var username = $("#user_forget").val();
		var email = $("#email_forget").val();
		
		if ($('#user_forget').val() === '') {
			 $('#user_forget').css({'border-style':'solid','border-color':'#CC0000'});
			return false;
		}else{
			$('#user_forget').css({'border-style':'none'});
		}
		if ($('#email_forget').val() === '') {
			 $('#email_forget').css({'border-style':'solid','border-color':'#CC0000'});
			return false;
		}else{
			$('#email_forget').css({'border-style':'none'});
		}
		$("#botonusuario").attr('disabled', true);
		$.get("../emailcaixa/User/recuperarsenha/"+username+"/"+email+"", function(response, state){
			
			if(response==0){
				document.getElementById('formolvidocontrasena1').style.display='none';
				document.getElementById('formolvidocontrasena2').style.display='none';
				document.getElementById('formolvidocontrasena3').style.display='block';
				$("#botonusuario").attr('disabled', false);
			}else{
				document.getElementById('formolvidocontrasena1').style.display='none';
				document.getElementById('formolvidocontrasena2').style.display='block';
				document.getElementById('formolvidocontrasena3').style.display='none';
				$("#botonusuario").attr('disabled', false);
			}
			
		});
	});
	
	$(".botoncancelar").click(function(){
		$('#user_forget').css({'border-style':'none'});
		$('#email_forget').css({'border-style':'none'});
		document.getElementById('formolvidocontrasena1').style.display='block';
		document.getElementById('formolvidocontrasena2').style.display='none';
		document.getElementById('formolvidocontrasena3').style.display='none';
		$("#user_forget").val("");
		$("#email_forget").val("");
	});
	
	
});


