$(function () {
    $('.js-basic-example').DataTable({
        dom: 'Bfrtip',
		responsive: true,
		'language': {
				'lengthMenu': 'Mostrar _MENU_',
				'zeroRecords': 'Sem registros para mostrar - Com licença',
				/*'info': 'P?gina _PAGE_ de _PAGES_',*/
				'infoEmpty': 'Nenhum registro disponível',
				'search': '_INPUT_',
				'searchPlaceholder': 'Pesquisar...',
				'paginate': {
				  'next': 'Seguinte',
				  'previous': 'Anterior'
				}
			},
		'bInfo' : false,
		'ordering': false,
		
    });

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});