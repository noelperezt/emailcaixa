<?php $band="criar"; $band2="state";?>
@extends('layouts.template')
	
	@section('title', 'Criar Estado')
	@section('content')
	
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">add_location</i>
					</div>
					<div class="content">
						<h3>Criar Estado</h3>
					</div>
				</div>
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					{!! Form::open(['route'=>'State.create', 'method'=>'POST']) !!}
						<div class="row clearfix">
								<div class="col-md-6">
                                    <p>
                                        <b>Nome</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <i class="material-icons">near_me</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" tabindex="0" name="state_name" id="state_name" placeholder="Nome" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <p>
                                        <b>Descrição</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <i class="material-icons">label_outline</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="description" id="description" placeholder="Descrição" required>
                                        </div>
                                    </div>
                                </div>
							<hr/>
							<div align="right">
								<button type="reset" class="btn btn-default waves-effect">
										<i class="material-icons">delete</i>
										<span>Apagar</span>
								</button>
								<button type="submit" class="btn bg-azul waves-effect">
										<i class="material-icons" style="color:white">add_location</i>
										<span style="color:white">Salvar</span>
								</button>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop