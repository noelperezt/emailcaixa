<?php $band="criar"; $band2="agency";?>
@extends('layouts.template')
	
	@section('title', 'Criar Agencia')
	@section('content')
	
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">location_city</i>
					</div>
					<div class="content">
						<h3>Criar Agencia</h3>
					</div>
				</div>
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					{!! Form::open(['route'=>'Agency.create', 'method'=>'POST']) !!}
						<div class="row clearfix">
                                <div class="col-md-4">
                                    <p>
                                        <b>CGC</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">picture_in_picture</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="cgc" id="cgc" placeholder="CGC" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Nome</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">local_offer</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="agency_name" id="agency_name" placeholder="Nome" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>N&deg;</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_1</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="agency_number" id="agency_number" placeholder="N&uacute;mero" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Unidade</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">my_location</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" tabindex="0" name="unity" id="unity" placeholder="Unidade" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>CEP</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">info_outline</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="cep" id="cep" placeholder="CEP" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>UF</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">flag</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="uf" id="uf" placeholder="UF" required>
                                        </div>
                                    </div>
                                </div>								
								<div class="col-md-4">
                                    <p>
                                        <b>Estado</b>
                                    </p>
									<div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">place</i>
                                        </span>
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="state_id" id="state_id" placeholder="Estado" required>
												<option value=""></option>
												@foreach($states as $state)
													@if($state->statu_id==1)
													<option value="{{$state->id}}">{{$state->state_name}}</option>
													@endif
												@endforeach
											</select>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Cidade</b>
                                    </p>
									<div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">room</i>
                                        </span>
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="city_id" id="city_id" placeholder="Cidade" required>
												
													<option value=""></option>
													
											</select>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Endereço</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">directions</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="address" id="address" placeholder="Endereço" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Bairro</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">near_me</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="neighborhood" id="neighborhood" placeholder="Bairro" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Telefone</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">local_phone</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="telephone" id="telephone" placeholder="Telefone" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<hr/>
							<div align="right">
								<button type="reset" class="btn btn-default waves-effect">
										<i class="material-icons">delete</i>
										<span>Apagar</span>
								</button>
								<button type="submit" class="btn bg-azul waves-effect">
										<i class="material-icons" style="color:white">location_city</i>
										<span style="color:white">Salvar</span>
								</button>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop