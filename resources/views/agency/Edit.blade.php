<?php $band="modificar"; $band2="agency";?>
@extends('layouts.template')
	
	@section('title', 'Modificar Agencia')
	@section('content')
		<?php  $host=$_SERVER['HTTP_HOST']; ?>
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">mode_edit</i>
					</div>
					<div class="content">
						<h3>Modificar Agencia - {{$agency->cep}}</h3>
					</div>
				</div>
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					<form action="http://<?php echo $host;?>/Agency/update" method="POST">
						<input type="hidden" id="id_agency" name="id_agency" value="{{$agency->id}}">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<div class="row clearfix">
                                <div class="col-md-4">
                                    <p>
                                        <b>CGC </b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">picture_in_picture</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$agency->cgc}}" class="form-control" name="cgc" id="cgc" placeholder="CGC" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Nome</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">local_offer</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="agency_name" value="{{$agency->agency_name}}"  id="agency_name" placeholder="Nome" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
									<p>
										<b>Status</b>
									</p>
									<div class="input-group input-group-sm">
										<span class="input-group-addon">
											<i class="material-icons">verified_user</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="id_statu" id="id_statu">
												@foreach($status as $statu)
													@if($statu->id==$agency->statu_id)
													   <option value="{{$statu->id}}" selected>{{$statu->status_name}}</option>
													@else
														@if($statu->module=='general')
															<option value="{{$statu->id}}">{{$statu->status_name}}</option>
														@endif
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
                                    <p>
                                        <b>N&deg;</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_1</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$agency->agency_number}}" class="form-control" name="agency_number" id="agency_number" placeholder="N&uacute;mero agencia" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Unidade</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">my_location</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$agency->unity}}" class="form-control" name="unity" id="unity" placeholder="Unidade" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>CEP</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">info_outline</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$agency->cep}}" class="form-control" name="cep" id="cep" placeholder="CEP" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>UF</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">flag</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$agency->uf}}" class="form-control" name="uf" id="uf" placeholder="UF" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
									<p>
										<b>Estado</b>
									</p>
									<div class="input-group input-group-sm">
										<span class="input-group-addon">
											<i class="material-icons">place</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="state_id2" id="state_id2" required>
												@foreach($states as $state)
													@if($state->id==$agency->state_id)
													   <option value="{{$state->id}}" selected>{{$state->state_name}}</option>
													@else
														<option value="{{$state->id}}">{{$state->state_name}}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<p>
										<b>Cidade</b>
									</p>
									<div class="input-group input-group-sm">
										<span class="input-group-addon">
											<i class="material-icons">room</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="city_id" id="city_id" required>
												@foreach($cities as $city)
													@if($city->id==$agency->city_id)
													   <option value="{{$city->id}}" selected>{{$city->city_name}}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
                                    <p>
                                        <b>Endereço</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">directions</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$agency->address}}" class="form-control" name="address" id="address" placeholder="Endereço" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Bairro</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">near_me</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$agency->neighborhood}}" class="form-control" name="neighborhood" id="neighborhood" placeholder="Bairro" required>
                                        </div>
                                    </div>
                                </div>								
								<div class="col-md-4">
                                    <p>
                                        <b>Telefone</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">local_phone</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$agency->telephone}}" class="form-control" name="telephone" id="telephone" placeholder="Telefone" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<hr/>
							<div align="right">
								<button type="submit" class="btn bg-azul waves-effect">
										<i class="material-icons" style="color:white">mode_edit</i>
										<span style="color:white">Salvar</span>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop