<?php $band="modificar"; $band2="agency";?>
@extends('layouts.template')
	
	@section('title', 'Agencias')
	@section('content')
	<?php  $host=$_SERVER['HTTP_HOST']; ?>
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">search</i>
					</div>
					<div class="content">
						<h3>Agencias</h3>
					</div>
					
				</div>
				
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					<a title="Adicione" href="http://<?php echo $host;?>/emailcaixa/Agency/create" class="btn btn-primary waves-effect waves-float" style="display: inline-block; margin: 0px auto; position: absolute; transition: all 0.5s ease-in-out; z-index: 1;">
						<i class="material-icons">playlist_add</i> Adicione
					</a>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
								<thead>
									<tr>
										<th>#</th>
										<th>CGC</th>
										<th>Nome</th>
										<th>Estado</th>
										<th>Cidade</th>
										<th>Endereço</th>
										<th>Status</th>
										<th>Modificar</th>
									</tr>
								</thead>
								<tbody>
									@foreach($agencies as $agency)
									<tr>
										<td>{{$agency->id}}</td>
										<td>{{$agency->cgc}}</td>
										<td>{{$agency->agency_name}}</td>
										<td>{{$agency->state->state_name}}</td>
										<td>{{$agency->city->city_name}}</td>
										<td>{{$agency->address }}</td>
										<td align="center">
										@if($agency->statu_id==1)
											<a name="" style="cursor: default;" class="btn btn-success waves-effect">Ativo</a>
										@endif
										@if($agency->statu_id==2)
											<a name="" style="cursor: default;" class="btn btn-danger waves-effect">Inativo</a>
										@endif
										</td>
										<td align="center"> 
											@if($agency->statu_id==1)
											<a id="{{$agency->id}}" name="{{$agency->id}}" title="Inativar" name="" class="btn inline btn-danger waves-effect waves-float delete_agency">
												<i id="{{$agency->id}}" class="material-icons" style="font-size:12px">block</i>
											</a>
											@endif
											@if($agency->statu_id==2)
											<a id="{{$agency->id}}" name="{{$agency->id}}" title="Ativar" name="" class="btn inline btn-success waves-effect waves-float active_agency">
												<i id="{{$agency->id}}" class="material-icons" style="font-size:12px">done_all</i>
											</a>
											@endif
											<a title="Editar" href="http://<?php echo $host;?>/emailcaixa/Agency/edit/{{$agency->id}}" class="btn inline btn-primary waves-effect waves-float">
												<i class="material-icons" style="font-size:12px">mode_edit</i>
											</a>										
										</td>
									</tr>
									@endforeach
								</tbody>
                            </table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop