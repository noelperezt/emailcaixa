@if(Session::has('message'))
	<?php $message=Session::get('message'); $message = explode(",", $message); ?>
<div  class="alert bg-<?php echo $message[0]; ?> alert-dismissible p-r-35 animated fadeInDown pull-right" role="alert" style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out; z-index: 1031; right:30px;">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<?php echo $message[1]; ?>
</div>
@endif
