<?php $band="criar"; $band2="user";?>
@extends('layouts.template')
	
	@section('title', 'Criar Usu&aacute;rios')
	@section('content')
	
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">person_add</i>
					</div>
					<div class="content">
						<h3>Criar Usu&aacute;rio</h3>
					</div>
				</div>
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					{!! Form::open(['route'=>'User.create', 'method'=>'POST']) !!}
						<div class="row clearfix">
                                <div class="col-md-4">
                                    <p>
                                        <b>Identidade </b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">picture_in_picture</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="document_number" id="document_number" placeholder="Identidade" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Nome</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" tabindex="0" name="first_name" id="first_name" placeholder="Nome" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Sobrenome</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Sobrenome" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Nome de usuário </b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person_add</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" id="username" placeholder="Nome de usuário" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Email</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Perfil</b>
                                    </p>
									<div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_open</i>
                                        </span>
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="id_role" id="id_role" placeholder="Perfil">
												@foreach($roles as $role)
													<option value="{{$role->id}}">{{$role->role_name}}</option>
												@endforeach
											</select>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Senha</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="password" id="password" placeholder="Senha" required  value="inicio01*" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<hr/>
							<div align="right">
								<button type="reset" class="btn btn-default waves-effect">
										<i class="material-icons">delete</i>
										<span>Apagar</span>
								</button>
								<button type="submit" class="btn bg-azul waves-effect">
										<i class="material-icons" style="color:white">person_add</i>
										<span style="color:white">Salvar</span>
								</button>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop