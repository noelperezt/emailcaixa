<?php $band="modificar"; $band2="user";?>
@extends('layouts.template')
	
	@section('title', 'Usu&aacute;rios')
	@section('content')
	<?php  $host=$_SERVER['HTTP_HOST']; ?>
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">search</i>
					</div>
					<div class="content">
						<h3>Usu&aacute;rios</h3>
					</div>
					
				</div>
				
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					<a title="Adicione" href="http://<?php echo $host;?>/emailcaixa/User/create" class="btn btn-primary waves-effect waves-float" style="display: inline-block; margin: 0px auto; position: absolute; transition: all 0.5s ease-in-out;z-index: 1;">
						<i class="material-icons">playlist_add</i> Adicione
					</a>
						<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Identidade</th>
										<th>Nome</th>
										<th>Usuário</th>
										<th>Email</th>
										<th>Perfil</th>
										<th>Estado</th>
										<th>Modificar</th>
									</tr>
								</thead>
								<tbody>
									@foreach($users as $user)
									<tr>
										<td>{{$user->id}}</td>
										<td>{{$user->document_number}}</td>
										<td>{{$user->first_name}} {{$user->last_name}}</td>
										<td>{{$user->username}}</td>
										<td>{{$user->email}}</td>
										<td>{{$user->role->role_name}}</td>
										<td align="center">
										@if($user->statu_id==1)
										   <button type="button" style="cursor: default;" class="btn btn-success waves-effect">Ativo</button>
										@else
											<button type="button" style="cursor: default;" class="btn btn-danger waves-effect">Inativo</button>
										@endif
										</td>
										<td align="center">
										@if($user->statu_id==1)
											<a id="{{$user->id}}" name="{{$user->id}}" title="Inativar" name="" class="btn inline btn-danger waves-effect waves-float delete_user">
												<i id="{{$user->id}}" class="material-icons" style="font-size:12px">block</i>
											</a>
										@endif
										@if($user->statu_id==2)
											<a id="{{$user->id}}" name="{{$user->id}}" title="Ativar" name="" class="btn inline btn-success waves-effect waves-float active_user">
												<i id="{{$user->id}}" class="material-icons" style="font-size:12px">done_all</i>
											</a>
										@endif
											
											<a title="Editar" href="http://<?php echo $host;?>/emailcaixa/User/edit/{{$user->id}}" class="btn inline btn-primary waves-effect waves-float">
												<i class="material-icons" style="font-size:12px">mode_edit</i>
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
                            </table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop