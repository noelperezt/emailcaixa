@extends('layouts.login')

	@section('title', 'Iniciar Sess&atilde;o')
	@section('content')
		@include('alerts.requestlogin')
		<div class="tile">
		  <div class="tile-header" id="particles-js">
			<div style="position:absolute; top:1%;left:35%;" align="center">
				<img src="{{{ asset('public/images/logoMenu branca.png') }}}" height="160" >
			</div>
		  </div>
		  
		  <div class="tile-body">
			{!! Form::open(['route'=>'log.store', 'method'=>'POST']) !!} 
			  <label class="form-input">
				<i class="material-icons">person</i> 
				<input type="text" autofocus="true" id="username" style="color:#999999" name="username"  required />
				<span class="label">Usu&aacute;rio</span>
				<span class="underline"></span>
			  </label>
			  
			  <label class="form-input">
				<i class="material-icons">lock</i>
				<input type="password" id="password" name="password"  required />
				<span class="label">Senha</span>
				<div class="underline"></div>
			  </label><br/>
			  <center>
				<a data-toggle="modal" href="#olvidocontrasena" id="forgot-pas" style="color:#666666">Recuperar Senha</a>
			  </center>
			  <div class="submit-container clearfix" style="margin-top: 2rem;">          
				 <button type="submit" class="btn btn-irenic float-right">ENTRAR</button>
			  </div>
			{!! Form::close() !!}
		  </div>
		  
		</div>
		
		<!-- Modal -->
		  <div aria-hidden="true" role="dialog" tabindex="-1" id="olvidocontrasena" class="modal fade">
			  <div class="modal-dialog" style="top:10%;z-index:3000; color:#333333">
				  <div class="modal-content">
					  <div class="modal-header" style="background-color:#000099">
						  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						  <h4 class="modal-title" style="color:white; font-size:18px">Recuperar Senha</h4>
					  </div>
					  <form  id="formolvidocontrasena1" name="formolvidocontrasena1">
						<div class="modal-body">
							 <label class="form-input">
								<i class="material-icons">person</i>
								<input type="text" id="user_forget" name="user_forget"  autofocus="true" required />
								<span class="label">Usu&aacute;rio</span>
								<span class="underline"></span>
							  </label>	
							  <label class="form-input">
								<i class="material-icons">email</i>
								<input type="text" id="email_forget" name="email_forget"  autofocus="true" required />
								<span class="label">Email</span>
								<span class="underline"></span>
							  </label>
						</div>
						<div class="modal-footer">
							<button data-dismiss="modal" class="btn btn-default botoncancelar" type="button">Voltar</button>
							<button id="botonusuario" name="botonusuario" class="btn btn-rojo" type="button">Enviar</button>
						</div>
					</form>
					<form  id="formolvidocontrasena2" name="formolvidocontrasena2" style="display:none">
						<div class="modal-body">
							<center>
								<i class="fa fa-check text-success" aria-hidden="true" style="font-size:150px;color:green"></i>
								<br/>Sua senha foi atualizada com sucesso.<br/>As informações de acesso foram enviadas para o seu correio.
							</center><br/>
						</div>
						<div class="modal-footer">
							<button data-dismiss="modal" class="btn btn-default botoncancelar" type="button">Aceptar</button>
						</div>
					</form>
					<form  id="formolvidocontrasena3" name="formolvidocontrasena3" style="display:none">
						<div class="modal-body">
							<center>
								<i class="fa  fa-times text-danger" aria-hidden="true" style="font-size:150px;color:red"></i>
								<br/>A informação inserida está incorreta.
							</center>	
						</div>
						<div class="modal-footer">
							<button data-dismiss="modal" class="btn btn-default botoncancelar" type="button">Aceptar</button>
						</div>
					</form>
				  </div>
			  </div>
		  </div>
		  <!-- modal -->	
	@stop