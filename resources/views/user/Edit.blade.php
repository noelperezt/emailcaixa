<?php $band="modificar"; $band2="user";?>
@extends('layouts.template')
	
	@section('title', 'Modificar Usu&aacute;rios')
	@section('content')
		<?php  $host=$_SERVER['HTTP_HOST']; ?>
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">mode_edit</i>
					</div>
					<div class="content">
						<h3>Modificar Usu&aacute;rio - {{$user->first_name}} {{$user->last_name}}</h3>
					</div>
				</div>
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					<?php if(Auth::user()->role_id == 1){ ?>
					<form action="http://<?php echo $host;?>/User/update" id="form" name="form" method="POST" data-toggle="validator" >
						<input type="hidden" id="id_user" name="id_user" value="{{$user->id}}">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<div class="row clearfix">
                                <div class="col-md-4">
                                    <p>
                                        <b>Identidade </b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">picture_in_picture</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$user->document_number}}" class="form-control" name="document_number" id="document_number" placeholder="Identidade" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Nome</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$user->first_name}}" class="form-control" name="first_name" id="first_name" placeholder="Nome" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Sobrenome</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$user->last_name}}" class="form-control" name="last_name" id="last_name" placeholder="Sobrenome" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Email</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="email" value="{{$user->email}}" class="form-control" name="email" id="email" placeholder="Email" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Nome de usuário </b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person_add</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$user->username}}" class="form-control" name="username" id="username" placeholder="Nome de usuário" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
									<p>
										<b>Estado</b>
									</p>
									<div class="input-group input-group-sm">
										<span class="input-group-addon">
											<i class="material-icons">verified_user</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="id_statu" id="id_statu">
												@foreach($status as $statu)
													@if($statu->id==$user->statu_id)
													   <option value="{{$statu->id}}" selected>{{$statu->status_name}}</option>
													@else
														<option value="{{$statu->id}}">{{$statu->status_name}}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
                                    <p>
                                        <b>Perfil</b>
                                    </p>
									<div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_open</i>
                                        </span>
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="id_role" id="id_role">
												@foreach($roles as $role)
													@if($role->id==$user->role_id)
													   <option value="{{$role->id}}" selected>{{$role->role_name}}</option>
													@else
														<option value="{{$role->id}}">{{$role->role_name}}</option>
													@endif
												@endforeach
											</select>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Senha</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="password" value="{{$user->password}}" class="form-control" name="password" id="password" placeholder="Senha" required disabled>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Confirmar Senha</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="password" value="{{$user->password}}" class="form-control" name="repassword" id="repassword" placeholder="Confirmar Senha" required disabled>
                                        </div>
										
                                    </div>
									<div align="right">
										<input type="checkbox" id="validapass"  value="1" name="validapass" />
										<label for="validapass" style="color:red">Marque para alterar a senha</label>
									</div>
                                </div>
								
								
                            </div>
							<div align="right">
								<table>
									<tr>
										<td>
											<span style="color:red;padding-right:25px; display:none" id="textmensaje" name="textmensaje">
											As senhas não correspondem
											</span>
										<td>
										<td>
											<button type="submit" id="save" name="save"  class="btn bg-azul waves-effect">
													<i class="material-icons" style="color:white">mode_edit</i>
													<span style="color:white">Salvar</span>
											</button>
										</td>
									</tr>
								</table>
							</div>
						</form>
						
					<?php }else{ ?>
						<form action="http://<?php echo $host;?>/User/updateuser" id="form" name="form" method="POST" data-toggle="validator" >
						<input type="hidden" id="id_user" name="id_user" value="{{$user->id}}">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<div class="row clearfix">
                                <div class="col-md-3">
                                    <p>
                                        <b>Identidade </b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">picture_in_picture</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$user->document_number}}" class="form-control" name="document_number" id="document_number" placeholder="Identidade" readonly>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <p>
                                        <b>Nome</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$user->first_name}}" class="form-control" name="first_name" id="first_name" placeholder="Nome" readonly>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <p>
                                        <b>Sobrenome</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$user->last_name}}" class="form-control" name="last_name" id="last_name" placeholder="Sobrenome" readonly>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <p>
                                        <b>Nome de usuário </b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person_add</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$user->username}}" class="form-control" name="username" id="username" placeholder="Nome de usuário" readonly>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Email</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="email" value="{{$user->email}}" class="form-control" name="email" id="email" placeholder="Email" required>
                                        </div>
                                    </div>
                                </div>
								
								<div class="col-md-4">
                                    <p>
                                        <b>Senha</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="password" value="{{$user->password}}" class="form-control" name="password" id="password" placeholder="Senha" required disabled>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Confirmar Senha</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="password" value="{{$user->password}}" class="form-control" name="repassword" id="repassword" placeholder="Confirmar Senha" required disabled>
                                        </div>
										
                                    </div>
									<div align="right">
										<input type="checkbox" id="validapass"  value="1" name="validapass" />
										<label for="validapass" style="color:red">Marque para alterar a senha</label>
									</div>
                                </div>
								
								
                            </div>
							<div align="right">
								<table>
									<tr>
										<td>
											<span style="color:red;padding-right:25px; display:none" id="textmensaje" name="textmensaje">
											As senhas não correspondem
											</span>
										<td>
										<td>
											<button type="submit" id="save" name="save"  class="btn bg-azul waves-effect">
													<i class="material-icons" style="color:white">mode_edit</i>
													<span style="color:white">Salvar</span>
											</button>
										</td>
									</tr>
								</table>
							</div>
						</form>
					<?php }?>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop