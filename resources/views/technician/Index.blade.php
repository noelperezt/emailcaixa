<?php $band="modificar"; $band2="technician";?>
@extends('layouts.template')
	
	@section('title', 'Técnicos')
	@section('content')
	<?php  $host=$_SERVER['HTTP_HOST']; ?>
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">search</i>
					</div>
					<div class="content">
						<h3>Técnicos</h3>
					</div>
					
				</div>
				
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					<a title="Adicione" href="http://<?php echo $host;?>/emailcaixa/Technician/create" class="btn btn-primary waves-effect waves-float" style="display: inline-block; margin: 0px auto; position: absolute; transition: all 0.5s ease-in-out;z-index: 1;">
						<i class="material-icons">playlist_add</i> Adicione
					</a>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Nome</th>
										<th>RG</th>
										<th>Telefone</th>
										<th>Parceiro</th>
										<th>Estado</th>
										<th>Modificar</th>
									</tr>
								</thead>
								<tbody>
									@foreach($technicians as $technician)
									<tr>
										<td>{{$technician->id}}</td>
										<td>{{$technician->technician_name}}</td>
										<td>{{$technician->rg}}</td>
										<td>{{$technician->telephone}}</td>
										<td>{{$technician->partner->partner_name}}</td>
										<td align="center">
										@if($technician->statu_id==1)
										   <button type="button" style="cursor: default;" class="btn btn-success waves-effect">Ativo</button>
										@else
											<button type="button" style="cursor: default;" class="btn btn-danger waves-effect">Inativo</button>
										@endif
										</td>
										<td align="center"> 
										@if($technician->statu_id==1)
											<a id="{{$technician->id}}" name="{{$technician->id}}" title="Inativar" name="" class="btn inline btn-danger waves-effect waves-float delete_technician">
												<i id="{{$technician->id}}" class="material-icons" style="font-size:12px">block</i>
											</a>
										@endif
										@if($technician->statu_id==2)
											<a id="{{$technician->id}}" name="{{$technician->id}}" title="Ativar" name="" class="btn inline btn-success waves-effect waves-float active_technician">
												<i id="{{$technician->id}}" class="material-icons" style="font-size:12px">done_all</i>
											</a>
										@endif
											
											<a title="Editar" href="http://<?php echo $host;?>/emailcaixa/Technician/edit/{{$technician->id}}" class="btn inline btn-primary waves-effect waves-float">
												<i class="material-icons" style="font-size:12px">mode_edit</i>
											</a>										
										</td>
									</tr>
									@endforeach
								</tbody>
                            </table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop