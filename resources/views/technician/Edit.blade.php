<?php $band="modificar"; $band2="technician";?>
@extends('layouts.template')
	
	@section('title', 'Modificar Técnicos')
	@section('content')
		<?php  $host=$_SERVER['HTTP_HOST']; ?>
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">mode_edit</i>
					</div>
					<div class="content">
						<h3>Modificar Técnicos - {{$technician->technician_name}}</h3>
					</div>
				</div>
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					<form action="http://<?php echo $host;?>/Technician/update" method="POST">
						<input type="hidden" id="id_technician" name="id_technician" value="{{$technician->id}}">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<div class="row clearfix">
                                <div class="col-md-4">
                                    <p>
                                        <b>Nome </b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$technician->technician_name}}" class="form-control" name="technician_name" id="technician_name" placeholder="Nome" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>RG</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_1</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$technician->rg}}" class="form-control" name="rg" id="rg" placeholder="RG" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
									<p>
										<b>Estado</b>
									</p>
									<div class="input-group input-group-sm">
										<span class="input-group-addon">
											<i class="material-icons">verified_user</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="id_statu" id="id_statu">
												@foreach($status as $statu)
													@if($statu->id==$technician->statu_id)
													   <option value="{{$statu->id}}" selected>{{$statu->status_name}}</option>
													@else
														@if($statu->module=='general')
															<option value="{{$statu->id}}">{{$statu->status_name}}</option>
														@endif
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
                                    <p>
                                        <b>Telefone</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">local_phone</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$technician->telephone}}" class="form-control" name="telephone" id="telephone" placeholder="Telefone" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
									<p>
										<b>Estado</b>
									</p>
									<div class="input-group input-group-sm">
										<span class="input-group-addon">
											<i class="material-icons">place</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="state_id2" id="state_id2" required>
												@foreach($states as $state)
													@if($state->id==$technician->state_id)
													   <option value="{{$state->id}}" selected>{{$state->state_name}}</option>
													@else
														@if($state->statu_id==1)
														<option value="{{$state->id}}">{{$state->state_name}}</option>
														@endif
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<p>
										<b>Cidade</b>
									</p>
									<div class="input-group input-group-sm">
										<span class="input-group-addon">
											<i class="material-icons">room</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="city_id" id="city_id" required>
												@foreach($cities as $city)
													@if($city->id==$technician->city_id)
													   <option value="{{$city->id}}" selected>{{$city->city_name}}</option>
													@else
														<option value="{{$city->id}}">{{$city->city_name}}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<p>
										<b>Parceiro</b>
									</p>
									<div class="input-group input-group-sm">
										<span class="input-group-addon">
											<i class="material-icons">business</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="partner_id" id="partner_id">
												@foreach($partners as $partner)
													@if($partner->id==$technician->partner_id)
													   <option value="{{$partner->id}}" selected>{{$partner->partner_name}}</option>
													@else
														@if($partner->statu_id==1)
														<option value="{{$partner->id}}">{{$partner->partner_name}}</option>
														@endif
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								
								
                            </div>
							<hr/>
							<div align="right">
								<button type="submit" class="btn bg-azul waves-effect">
										<i class="material-icons" style="color:white">mode_edit</i>
										<span style="color:white">Salvar</span>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop