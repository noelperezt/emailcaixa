<?php $band="modificar"; $band2="city";?>
@extends('layouts.template')
	
	@section('title', 'Modificar Cidades')
	@section('content')
		<?php  $host=$_SERVER['HTTP_HOST']; ?>
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">mode_edit</i>
					</div>
					<div class="content">
						<h3>Modificar Cidade - {{$city->city_name}}</h3>
					</div>
				</div>
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					<form action="http://<?php echo $host;?>/City/update" method="POST">
						<input type="hidden" id="id_city" name="id_city" value="{{$city->id}}">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<div class="row clearfix">
                                <div class="col-md-4">
                                    <p>
                                        <b>Nome</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">near_me</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$city->city_name}}" class="form-control" name="city_name" id="city_name" placeholder="Nome" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
									<p>
										<b>Estado</b>
									</p>
									<div class="input-group input-group-sm">
										<span class="input-group-addon">
											<i class="material-icons">place</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="state_id2" id="state_id2">
												@foreach($states as $state)
													@if($state->id==$city->state_id)
													   <option value="{{$state->id}}" selected>{{$state->state_name}}</option>
													@else
														<option value="{{$state->id}}">{{$state->state_name}}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<p>
										<b>Status</b>
									</p>
									<div class="input-group input-group-sm">
										<span class="input-group-addon">
											<i class="material-icons">verified_user</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="id_statu" id="id_statu">
												@foreach($status as $statu)
													@if($statu->id==$city->statu_id)
													   <option value="{{$statu->id}}" selected>{{$statu->status_name}}</option>
													@else
														@if($statu->module=='general')
															<option value="{{$statu->id}}">{{$statu->status_name}}</option>
														@endif
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12">
                                    <p>
                                        <b>Descrição</b>
                                    </p>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">
                                            <i class="material-icons">label_outline</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$city->description}}" class="form-control" name="description" id="description" placeholder="Descrição" required>
                                        </div>
                                    </div>
                                </div>
								
								
                            </div>
							<hr/>
							<div align="right">
								<button type="submit" class="btn bg-azul waves-effect">
										<i class="material-icons" style="color:white">mode_edit</i>
										<span style="color:white">Salvar</span>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop