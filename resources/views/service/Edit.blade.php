<?php $band="modificar"; $band2="service";?>
@extends('layouts.template')
	
	@section('title', 'Modificar Pedido')
	@section('content')
		<?php  $host=$_SERVER['HTTP_HOST']; ?>
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">mode_edit</i>
					</div>
					<div class="content">
						<h3>Modificar Pedido - {{$service->id}}</h3>
					</div>
				</div>
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					<form action="http://<?php echo $host;?>/Service/update" method="POST">
						<input type="hidden" id="id_service" name="id_service" value="{{$service->id}}">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<div class="row clearfix">
                                <div class="col-md-3">
                                    <p>
                                        <b>Data</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <i class="material-icons">event</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" readonly value="<?php echo date("d/m/Y", strtotime($service->service_date)); ?>">
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <p>
                                        <b>Hora</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        <div class="form-line">
                                            <input type="text" class="form-control" readonly value="{{$service->service_time}}">
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <p>
                                        <b>Data de criação</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <i class="material-icons">event</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" readonly value="<?php echo date("d/m/Y", strtotime($service->created_at)); ?>">
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-3">
									<p>
										<b>Status</b>
									</p>
									<div class="input-group input-group-lg">
										<span class="input-group-addon">
											<i class="material-icons">verified_user</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="id_statu" id="id_statu" required>
												@foreach($status as $statu)
													@if($statu->id==$service->statu_id)
													   <option value="{{$statu->id}}" selected>{{$statu->status_name}}</option>
													@else
														@if($statu->module=='service' && $service->statu_id=='3')
														<option value="{{$statu->id}}">{{$statu->status_name}}</option>
														@endif
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12">
                                    <p>
                                        <b>Descrição</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" value="{{$service->description}}" class="form-control" name="description" id="description" placeholder="Descrição">
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Agencia</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <i class="material-icons">location_city</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" readonly value="{{$service->agency->agency_number}} - {{$service->agency->unity}}">
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>Parceiro</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <i class="material-icons">business</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" readonly value="{{$service->partner->partner_name}}">
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <p>
                                        <b>T&eacute;cnico</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" readonly value="{{$service->technician->technician_name}}">
                                        </div>
                                    </div>
                                </div>
								
                            </div>
							<div align="right">
								<button type="submit" class="btn bg-azul waves-effect">
										<i class="material-icons" style="color:white">mode_edit</i>
										<span style="color:white">Salvar</span>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop