<?php $band="modificar"; $band2="service";?>
@extends('layouts.template')
	
	@section('title', 'Pedidos')
	@section('content')
	<?php  $host=$_SERVER['HTTP_HOST']; ?>
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">search</i>
					</div>
					<div class="content">
						<h3>Pedidos</h3>
					</div>
					
				</div>
				
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					<a title="Adicione" href="http://<?php echo $host;?>/emailcaixa/Service/create" class="btn btn-primary waves-effect waves-float" style="display: inline-block; margin: 0px auto; position: absolute; transition: all 0.5s ease-in-out;z-index: 1;">
						<i class="material-icons">playlist_add</i> Adicione
					</a>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Data</th>
										<th>Agencia</th>
										<th>Parceiro</th>
										<th>Técnico</th>
										<th>Cidade/Estado</th>
										<th>Status</th>
										<th>Modificar</th>
									</tr>
								</thead>
								<tbody>
									@foreach($services as $service)
									<tr style="font-size:12px">
										<td>{{$service->id}}</td>
										<td><?php echo date("d/m/Y", strtotime($service->service_date)); ?> - {{$service->service_time}}</td>
										<td>{{$service->agency->agency_name}}</td>
										<td>{{$service->partner->partner_name}}</td>
										<td>{{$service->technician->technician_name}}</td>
										<td>{{$service->agency->city->city_name}}/{{$service->agency->state->state_name}}</td>
										<td align="center">
										@if($service->statu_id==3)
										   <button type="button" style="cursor: default;" class="btn btn-default waves-effect">Criado</button>
										@endif
										@if($service->statu_id==4)
										   <button type="button" style="cursor: default;" class="btn btn-success waves-effect">Terminado</button>
										@endif
										@if($service->statu_id==5)
										   <button type="button" style="cursor: default;" class="btn btn-danger waves-effect">Cancelado</button>
										@endif
										</td>
										<td align="center"> 
										@if($service->statu_id==3)
											<a id="{{$service->id}}" name="{{$service->id}}" title="Terminado" name="" class="btn inline btn-success waves-effect waves-float success_service">
												<i id="{{$service->id}}" class="material-icons" style="font-size:12px">done_all</i>
											</a>
											<a id="{{$service->id}}" name="{{$service->id}}" title="Cancelado" name="" class="btn inline btn-danger waves-effect waves-float cancel_service">
												<i id="{{$service->id}}" class="material-icons" style="font-size:12px">clear</i>
											</a>
										@endif
											<a title="Editar" href="http://<?php echo $host;?>/emailcaixa/Service/edit/{{$service->id}}" class="btn inline btn-primary waves-effect waves-float">
												<i class="material-icons" style="font-size:12px">mode_edit</i>
											</a>										
										</td>
									</tr>
									@endforeach
								</tbody>
                            </table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
	@stop