<?php $band="criar"; $band2="service";?>
@extends('layouts.template2')
	
	@section('title', 'Criar Pedido')
	@section('content')
	<script>
	  $( function() {
		$( "#service_date" ).datepicker();
	  } );
	 </script>
	
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box bg-azul hover-expand-effect">
					<div class="icon">
						<i class="material-icons">person_pin_circle</i>
					</div>
					<div class="content">
						<h3>Criar Pedido</h3>
					</div>
				</div>
				<div class="card" style="margin:0px;padding:0px;top:-25px;">
					@include('alerts.request')
					<div class="body">
					{!! Form::open(['route'=>'Service.create', 'method'=>'POST']) !!}
						<div class="row clearfix">
								<div class="col-md-2">
                                    <p>
                                        <b>Data</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <i class="material-icons">event</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="service_date" id="service_date" required readonly>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-2">
                                    <p>
                                        <b>Hora</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        
                                        <div class="form-line">
											<select class="form-control show-tick" name="service_time" id="service_time" required>
												<option value=""></option>
												<option value="08:00 am">08:00 am</option>
												<option value="09:00 am">09:00 am</option>
												<option value="10:00 am">10:00 am</option>
												<option value="11:00 am">11:00 am</option>
												<option value="12:00 pm">12:00 pm</option>
												<option value="01:00 pm">01:00 pm</option>
												<option value="02:00 pm">02:00 pm</option>
												<option value="03:00 pm">03:00 pm</option>
												<option value="04:00 pm">04:00 pm</option>
												<option value="05:00 pm">05:00 pm</option>
											</select>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-8">
                                    <p>
                                        <b>Descrição</b>
                                    </p>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <i class="material-icons">rate_review</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="description" id="description" placeholder="Descrição" required>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-4">
									<p>
										<b>Agencia</b>
									</p>
									<div class="input-group input-group-lg">
										<span class="input-group-addon">
											<i class="material-icons">location_city</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="agency_id" id="agency_id" required>
												<option value=""></option>
												@foreach($agencies as $agency)
													@if($agency->statu_id==1)
													<option value="{{$agency->id}}">{{$agency->cgc}} - {{$agency->address}}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<p>
										<b>Parceiro</b>
									</p>
									<div class="input-group input-group-lg">
										<span class="input-group-addon">
											<i class="material-icons">business</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="partner_id" id="partner_id" required>
												<option value=""></option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<p>
										<b>T&eacute;cnico</b>
									</p>
									<div class="input-group input-group-lg">
										<span class="input-group-addon">
											<i class="material-icons">person</i>
										</span>
										<div class="form-line">
											<select class="form-control show-tick" name="technician_id" id="technician_id" required>
												<option value=""></option>
											</select>
										</div>
									</div>
								</div>
								
								<div align="right" class="col-md-12">
								<hr/>
									<button type="reset" class="btn btn-default waves-effect">
											<i class="material-icons">delete</i>
											<span>Apagar</span>
									</button>
									<button type="submit" class="btn bg-azul waves-effect">
											<i class="material-icons" style="color:white">add_location</i>
											<span style="color:white">Salvar</span>
									</button>
								</div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Widgets -->
		<div style="display:none">
		<button type="button" id="alertaParceiro" name="alertaParceiro" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#ParceirosModal">Alerta</button>
		<button type="button" id="alertaTecnico" name="alertaTecnico" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#TecnicosModal">Alerta</button>
		</div>
		<!-- Modal Dialogs ====================================================================================================================== -->
            <!-- Default Size -->
            <div class="modal fade" id="ParceirosModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color:#000099">
						  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						  <table><tr><td><i class="material-icons" style="color:#FFFF66">business</i></td><td>
						  <h4 class="modal-title" style="color:white; font-size:18px; margin-left=15px;padding-left:20px;">Informação</h4></td></tr></table>				
					  </div>
                        <div class="modal-body">
                           <center>
								<i class="material-icons" style="font-size:150px;color:#FFFF66">warning</i>
								<p>Não há parceiros na cidade da agência, os parceiros do estado estão listados.</p>
							</center><br/><br/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
			<div class="modal fade" id="TecnicosModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color:#000099">
						  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						  <table><tr><td><i class="material-icons" style="color:#FFFF66">person</i></td><td>
						  <h4 class="modal-title" style="color:white; font-size:18px; margin-left=15px;padding-left:20px;">Informação</h4></td></tr></table>				
					  </div>
                        <div class="modal-body">
                           <center>
								<i class="material-icons" style="font-size:150px;color:#FFFF66">warning</i>
								<p>O técnico selecionado tem serviços programados para a data selecionada, no entanto, você pode continuar com o pedido</p>
							</center><br/><br/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
		
	@stop