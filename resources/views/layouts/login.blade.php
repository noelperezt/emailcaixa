<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Alfa Telecom - @yield('title')</title>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
{!! Html::style('public/css/style.particles.css') !!}
{!! Html::style('public/css/style.login.css') !!}
{!! Html::style('public/css/bootstrap/bootstrap.modal.css') !!}
{!! Html::style('public/css/style.particles.css') !!}

<!-- Custom Css -->
{!! Html::style('public/css/style.principal.css') !!}


<link rel="shortcut icon" type="image/x-icon" href="{{{ asset('public/images/favicon-32.png') }}}">
</head>
<body>
@yield('content')
<div style="position:absolute;bottom:10px; left:40px;" align="left">
<div class="textowhite2"><strong style="font-size:27px">SAC 4020-9933</strong></div>
</div>
<div style="position:absolute;bottom:10px; right:40px;" align="left">
<div class="textowhite2">sac@alfatelecom.info&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;www.alfatelecom.info</div>
</div>
{!! Html::script('public/js/particles/particles.js') !!}
{!! Html::script('public/js/particles/app.particles.js') !!}
{!! Html::script('public/js/jquery.min.js') !!}
{!! Html::script('public/js/bootstrap/bootstrap.min.js') !!}
{!! Html::script('public/js/app.ajax.js') !!}

</body>
</html>
