<!DOCTYPE html>
<?php  $host=$_SERVER['HTTP_HOST']; ?>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Alfa Telecom - @yield('title')</title>
    <!-- Favicon-->
    <link rel="shortcut icon" type="image/x-icon" href="{{{ asset('public/images/favicon-32.png') }}}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core Css -->
	{!! Html::style('public/css/bootstrap/bootstrap.css') !!}
	{!! Html::style('public/css/bootstrap-select/css/bootstrap-select.css') !!}
	
	<!-- Datatables Css -->
	{!! Html::style('public/js/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') !!}
	
    <!-- Waves Effect Css -->
	{!! Html::style('public/css/node-waves/waves.css') !!}
    
    <!-- Animation Css -->
	{!! Html::style('public/css/animate-css/animate.css') !!}
    
    <!-- Morris Chart Css-->
	{!! Html::style('public/css/morrisjs/morris.css') !!}
    
    <!-- Custom Css -->
	{!! Html::style('public/css/style.principal.css') !!}
    
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
	{!! Html::style('public/css/themes/all-themes.css') !!}
</head>

<body class="theme-blue">

	<script>
		
		jQuery(function($){
        $.datepicker.regional['pt-BR'] = {
                closeText: 'Fechar',
                prevText: '',
                nextText: '',
                currentText: 'Hoje',
                monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
                'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
                'Jul','Ago','Set','Out','Nov','Dez'],
                dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
                dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
		});
	</script>
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-indigo">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Por favor, espere...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="http://<?php echo $host;?>/emailcaixa/Home"><img src="{{{ asset('public/images/Logotipo3.png') }}}" height="65" style="margin:0px;padding:0px;position:absolute;top:2px;"></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
						<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
						<span class="material-text" style="position:relative;top:5px;padding-left:2px;padding-right:5px;font-size:16px;">Bem-vindo,</span>
						<span class="material-text" style="position:relative;top:5px;font-weight:700;padding-left:2px;padding-right:50px;font-size:16px;">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</span>
						</a>
					</li>
                    <!-- Tasks -->
                    <li>
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">place</i>
							<span class="material-text" style="position:relative;top:-5px;font-weight:700;padding-left:2px;padding-right:50px;">MANAUS/AM</span>
                        </a>
                    </li>
					<li>
                        <a href="http://<?php echo $host;?>/emailcaixa/logout" role="button" title="Sair">
                            <i class="material-icons" >exit_to_app</i>
                        </a>
                    </li>
                    <!-- #END# Tasks -->
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">

            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li <?php if($band=='principal'){echo 'class="active"';} ?> >
                        <a href="http://<?php echo $host;?>/emailcaixa/Home">
                            <i class="material-icons">home</i>
                            <span>Principal</span>
                        </a>
                    </li>
					<li <?php if($band2=='agency'){echo 'class="active"';} ?> >
                        <a href="http://<?php echo $host;?>/emailcaixa/Agency">
                            <i class="material-icons">location_city</i>
                            <span>Agencias</span>
                        </a>
                    </li>
					<li <?php if($band2=='city'){echo 'class="active"';} ?> >
                        <a href="http://<?php echo $host;?>/emailcaixa/City">
                            <i class="material-icons">add_location</i>
                            <span>Cidades</span>
                        </a>
                    </li>
					<li <?php if($band2=='state'){echo 'class="active"';} ?> >
                        <a href="http://<?php echo $host;?>/emailcaixa/State">
                            <i class="material-icons">my_location</i>
                            <span>Estados</span>
                        </a>
                    </li>
					<li <?php if($band2=='partner'){echo 'class="active"';} ?> >
                        <a href="http://<?php echo $host;?>/emailcaixa/Partner">
                            <i class="material-icons">business</i>
                            <span>Parceiros</span>
                        </a>
                    </li>
					<li <?php if($band2=='service'){echo 'class="active"';} ?> >
                        <a href="http://<?php echo $host;?>/emailcaixa/Service">
                            <i class="material-icons">person_pin</i>
                            <span>Serviços</span>
                        </a>
                    </li>
					<li <?php if($band2=='technician'){echo 'class="active"';} ?> >
                        <a href="http://<?php echo $host;?>/emailcaixa/Technician">
                            <i class="material-icons">settings</i>
                            <span>T&eacute;cnicos</span>
                        </a>
                    </li>
					<?php if(Auth::user()->role_id == 1){ ?>
					<li <?php if($band2=='user'){echo 'class="active"';} ?> >
                        <a href="http://<?php echo $host;?>/emailcaixa/User">
                            <i class="material-icons">person_add</i>
                            <span>Usu&aacute;rios</span>
                        </a>
                    </li>
					<?php }else{ ?>
					<li <?php if($band2=='user'){echo 'class="active"';} ?> >
                        <a href="http://<?php echo $host;?>/emailcaixa/User/edit/<?php echo Auth::user()->id; ?>">
                            <i class="material-icons">person_add</i>
                            <span>Usu&aacute;rio</span>
                        </a>
                    </li>
					<?php }?>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright" align="center">
                    &copy; 2018 <a href="javascript:void(0);">Alfa Telecom</a>
                </div>
                <!--<div class="version">
                    <b>Version: </b> 1.0.5
                </div>-->
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid" >
           @yield('content')
        </div>
    </section>

    <!-- Jquery Core Js -->
	{!! Html::script('public/js/jquery.min.js') !!}
	{!! Html::script('public/js/jquery.maskedinput.js') !!}
	{!! Html::script('public/js/inputmask.js') !!}
	
    <!-- Bootstrap Core Js -->
	{!! Html::script('public/js/bootstrap/bootstrap.js') !!}
	
   
    <!-- Slimscroll Plugin Js -->
	{!! Html::script('public/js/jquery-slimscroll/jquery.slimscroll.js') !!}
	
    <!-- Waves Effect Plugin Js -->
	{!! Html::script('public/js/node-waves/waves.js') !!}
    
	<!-- Datatables Plugin Js -->
	{!! Html::script('public/js/jquery-datatable/jquery.dataTables.js') !!}
	{!! Html::script('public/js/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') !!}
	
    <!-- Jquery CountTo Plugin Js 
	{!! Html::script('js/jquery-countto/jquery.countTo.js') !!}-->

    <!-- Morris Plugin Js 
	{!! Html::script('js/raphael/raphael.min.js') !!}
	{!! Html::script('js/morrisjs/morris.js') !!}-->

    <!-- ChartJs
	{!! Html::script('js/chartjs/Chart.bundle.js') !!} -->
    
    <!-- Flot Charts Plugin Js
	{!! Html::script('js/flot-charts/jquery.flot.js') !!}
	{!! Html::script('js/flot-charts/jquery.resize.js') !!}
	{!! Html::script('js/flot-charts/jquery.pie.js') !!}
	{!! Html::script('js/flot-charts/jquery.categories.js') !!}
	{!! Html::script('js/flot-charts/jquery.time.js') !!} -->

    <!-- Sparkline Chart Plugin Js 
	{!! Html::script('js/jquery-sparkline/jquery.sparkline.js') !!}-->
    
    <!-- Custom Js -->
	{!! Html::script('public/js/admin.js') !!}
	{!! Html::script('public/js/jquery-datatable.js') !!}
	{!! Html::script('public/js/dropdown.js') !!}
	{!! Html::script('public/js/app.ajax.js') !!}
	{!! Html::script('public/js/pages/index.js') !!}
	
    <!-- Demo Js -->
	{!! Html::script('public/js/demo.js') !!}
</body>

</html>