<?php $host=$_SERVER['HTTP_HOST']; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<p>
	Prezados Senhores,<br/>
	Solicito autorização para a equipo da Alfa Telecom que irá realizar reparos para os serviços dos links de dados na Caixa Ecônomica Federal conforme abaixo:</p><br/>
	
	<table style='width:100%;'>
		<thead>		
			<tr>
				<th style="width:30%;text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;background-color:#F7F7F7; border-right-color:#CCCCCC;border-right-style:solid; border-right-width:thin !important;border-top-color:#DDDDDD !important; border-top-style:solid !important; border-top-width:thin !important;border-left-color:#CCCCCC; border-left-style:solid; border-left-width:thin;">CGC</th>
				<th style="width:30%;text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;background-color:#F7F7F7; border-right-color:#CCCCCC;border-right-style:solid; border-right-width:thin !important;border-top-color:#DDDDDD !important; border-top-style:solid !important; border-top-width:thin !important;border-left-color:#CCCCCC; border-left-style:solid; border-left-width:thin;">UNIDADE</th>
				<th style="width:30%;text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;background-color:#F7F7F7; border-right-color:#CCCCCC;border-right-style:solid; border-right-width:thin !important;border-top-color:#DDDDDD !important; border-top-style:solid !important; border-top-width:thin !important;border-left-color:#CCCCCC; border-left-style:solid; border-left-width:thin;">ENDEREÇO</th>
				<th style="width:30%;text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;background-color:#F7F7F7; border-right-color:#CCCCCC;border-right-style:solid; border-right-width:thin !important;border-top-color:#DDDDDD !important; border-top-style:solid !important; border-top-width:thin !important;border-left-color:#CCCCCC; border-left-style:solid; border-left-width:thin;">BAIRRO</th>
				<th style="width:30%;text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;background-color:#F7F7F7; border-right-color:#CCCCCC;border-right-style:solid; border-right-width:thin !important;border-top-color:#DDDDDD !important; border-top-style:solid !important; border-top-width:thin !important;border-left-color:#CCCCCC; border-left-style:solid; border-left-width:thin;">CIDADE</th>
				<th style="width:30%;text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;background-color:#F7F7F7; border-right-color:#CCCCCC;border-right-style:solid; border-right-width:thin !important;border-top-color:#DDDDDD !important; border-top-style:solid !important; border-top-width:thin !important;border-left-color:#CCCCCC; border-left-style:solid; border-left-width:thin;">CEP</th>
				<th style="width:30%;text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;background-color:#F7F7F7; border-right-color:#CCCCCC;border-right-style:solid; border-right-width:thin !important;border-top-color:#DDDDDD !important; border-top-style:solid !important; border-top-width:thin !important;border-left-color:#CCCCCC; border-left-style:solid; border-left-width:thin;">UF</th>				
			</tr>
			<tr>
				<td style="text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;border-right-color:#CCCCCC; border-right-style:solid; border-right-width:thin;">{!!$cgc!!}</td>
				<td style="text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;border-right-color:#CCCCCC; border-right-style:solid; border-right-width:thin;">{!!$unity!!}</td>
				<td style="text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;border-right-color:#CCCCCC; border-right-style:solid; border-right-width:thin;">{!!$address!!}</td>
				<td style="text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;border-right-color:#CCCCCC; border-right-style:solid; border-right-width:thin;">{!!$neighborhood!!}</td>
				<td style="text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;border-right-color:#CCCCCC; border-right-style:solid; border-right-width:thin;">{!!$city_name!!}</td>
				<td style="text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;border-right-color:#CCCCCC; border-right-style:solid; border-right-width:thin;">{!!$cep!!}</td>
				<td style="text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;border-right-color:#CCCCCC; border-right-style:solid; border-right-width:thin;">{!!$uf!!}</td>
			</tr>
								
		</thead>
	</table><br>
	<p>
	<strong>Técnico:</strong> {!!$technician_name!!} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>RG:</strong> {!!$rg!!}
	</p><br>
	<p>
	Os serviços serão realizados hoje dia {!!$date!!} durante o horário comercial.<br/>
	Favor confirmar o recebimento deste e-mail e nos informar se houver algum impedimento.
	</p><br>
	<p> 
	Atenciosamente, <br>
	<b><a style="color:#000099" href="http://<?php echo $host; ?>/" >Alfa Telecom</a></b><br>
	Rua Portugal , 69 Japiim - AM<br>
	Telefone: (92) 4020-9933<br>
	Email: soporte12@alfatelecom.info<br>
	</p>
</body>
</html>