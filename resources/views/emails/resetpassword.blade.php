<?php $host=$_SERVER['HTTP_HOST']; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<p>
	Prezado <strong>{!!$first_name!!} {!!$last_name!!}</strong>,<br/>
	Seu pedido de recuperação de senha foi respondido satisfatoriamente. Suas informações de acesso são:</p>
	<p style='padding-bottom: 20px;border-bottom: 3px solid #000099;margin-bottom: 10px;'></p>	
	
	<table style='width:100%;'>
		<thead>		
			<tr>
				<th style="width:30%;text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;background-color:#F7F7F7; border-right-color:#CCCCCC;border-right-style:solid; border-right-width:thin !important;border-top-color:#DDDDDD !important; border-top-style:solid !important; border-top-width:thin !important;border-left-color:#CCCCCC; border-left-style:solid; border-left-width:thin;">Nome Usuário:</th>
				<td style="text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;border-right-color:#CCCCCC; border-right-style:solid; border-right-width:thin;">{!!$username!!}</td>
			</tr>
			<tr>
				<th style="width:30%;text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;background-color:#F7F7F7; border-right-color:#CCCCCC;border-right-style:solid; border-right-width:thin !important;border-top-color:#DDDDDD !important; border-top-style:solid !important; border-top-width:thin !important;border-left-color:#CCCCCC; border-left-style:solid; border-left-width:thin;">Senha:</th>
				<td style="text-align: left;padding: 8px;border-top: 1px solid #ddd;border-bottom: 2px solid #ddd;border-right-color:#CCCCCC; border-right-style:solid; border-right-width:thin;">{!!$password!!}</td>
			</tr>
								
		</thead>
	</table><p style='margin-bottom:30px;border-bottom:2px dashed #000099;'></p><br>

	<p> 
	Sinceramente, <br>
	<b><a style="color:#000099"  href="http://<?php echo $host; ?>/">Alfa Telecom</a></b><br>
	Rua Portugal , 69 Japiim - AM<br>
	Telefone: (92) 4020-9933<br>
	Email: soporte12@alfatelecom.info<br>
	</p>
</body>
</html>